package com.example.irrigationsystem.config;

import com.example.irrigationsystem.model.SensorData;
import com.example.irrigationsystem.repo.SensorDataRepository;
import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;
import java.util.logging.Logger;


@Configuration
public class MqttConfig {

    private static final Logger LOGGER = Logger.getLogger(MqttConfig.class.getName());

    @Autowired
    private SensorDataRepository sensorDataRepository;

    private final String brokerUrl = "tcp://35.240.75.59:1883"; // Replace with your broker address
    private final String clientId = "spring-boot-mqtt-client";

    @Bean
    public MqttClient mqttClient() throws MqttException {
        MqttConnectOptions options = new MqttConnectOptions();
        options.setCleanSession(true);
        options.setAutomaticReconnect(true);

        MqttClient client = new MqttClient(brokerUrl, clientId);
        client.connect(options);

        // Subscribe to topics with wildcard for dynamic parcel names
        client.subscribe("+/+/+/soil_moisture", createMessageListener("Soil Moisture"));
        client.subscribe("+/+/+/soil_temperature", createMessageListener("Soil Temperature"));
        client.subscribe("+/+/+/evapotranspiration", createMessageListener("Evapotranspiration"));

        return client;
    }

    private IMqttMessageListener createMessageListener(String sensorName) {
        return (topic, msg) -> {
            String[] topicParts = topic.split("/");
            if (topicParts.length >= 4) {
                String userName = topicParts[0];
                String corpName = topicParts[1];
                String parcelName = topicParts[2];
                String data = new String(msg.getPayload());
                LocalDateTime timestamp = LocalDateTime.now();

                SensorData sensorData = new SensorData();
                sensorData.setUserName(userName);
                sensorData.setCorpName(corpName);
                sensorData.setParcelName(parcelName);
                sensorData.setSensorName(sensorName);
                sensorData.setData(data);
                sensorData.setTimestamp(timestamp);

                sensorDataRepository.save(sensorData);

                System.out.println("Received " + sensorName + " data for user " + userName + ", corp " + corpName + ", parcel " + parcelName + ": " + data + " at " + timestamp);
            } else {
                System.out.println("Invalid topic format: " + topic);
            }
        };
    }


}
