package com.example.irrigationsystem.config;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class UserActionLoggerAspect {

    private final Logger logger = LoggerFactory.getLogger(UserActionLoggerAspect.class);

    @Around("@annotation(org.springframework.security.access.annotation.Secured)")
    public Object logUserAction(ProceedingJoinPoint joinPoint) throws Throwable {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            String username = authentication.getName();
            String methodName = joinPoint.getSignature().getName();
            Object[] args = joinPoint.getArgs();

            // Log details about the user action (username, method, arguments)
            logger.info(String.format("User '%s' performed action: %s with arguments: %s", username, methodName, args));
        } else {
            // Log that no user is authenticated
            logger.warn("No user authenticated.");
        }

        // Proceed with the method execution
        return joinPoint.proceed();
    }
}