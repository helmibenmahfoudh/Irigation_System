package com.example.irrigationsystem.controller;


import com.example.irrigationsystem.config.jwt.JwtUtils;
import com.example.irrigationsystem.model.ERole;
import com.example.irrigationsystem.model.Role;
import com.example.irrigationsystem.model.User;
import com.example.irrigationsystem.payload.request.LoginRequest;
import com.example.irrigationsystem.payload.request.SignupRequest;
import com.example.irrigationsystem.payload.response.JwtResponse;
import com.example.irrigationsystem.payload.response.MessageResponse;
import com.example.irrigationsystem.payload.response.UserResponse;
import com.example.irrigationsystem.repo.RoleRepository;
import com.example.irrigationsystem.repo.UserRepository;
import com.example.irrigationsystem.services.UserDetailsImpl;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;



@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;


    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());

        UserResponse userResponse = new UserResponse(userDetails.getId(),userDetails.getUsername(),userDetails.getEmail());
        userResponse.setId(userDetails.getId());
        userResponse.setUsername(userDetails.getUsername());
        userResponse.setEmail(userDetails.getEmail());


        return ResponseEntity.ok(new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getEmail(),
                roles));
    }


    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {

        // Retrieve the currently logged-in user's authentication context
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        // Extract the UserDetailsImpl object from the authentication context
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        // Extract the role of the logged-in user
        String currentRole = userDetails.getAuthorities().stream().findFirst().orElse(null).getAuthority();

        // Extract the username of the logged-in user
        String currentUsername = userDetails.getUsername();

        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }

        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use!"));
        }

        // Create new user's account
        User user = new User(signUpRequest.getUsername(),
                signUpRequest.getEmail(),
                encoder.encode(signUpRequest.getPassword()),
                signUpRequest.getFirstName(),
                signUpRequest.getLastName(),
                currentUsername);

        Set<Role> roles = new HashSet<>();
        if ("ADMIN".equals(currentRole)) {
            // If the logged-in user is an ADMIN, assign FARMER role to the new user
            user.setRoles(roleRepository.findByName(ERole.FARMER));
        } else if ("FARMER".equals(currentRole)) {
            // If the logged-in user is a FARMER, assign EMPLOYEE role to the new user
            user.setRoles(roleRepository.findByName(ERole.EMPLOYEE));
        }
        userRepository.save(user);

        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }
}