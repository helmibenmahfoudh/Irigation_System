package com.example.irrigationsystem.controller;

import com.example.irrigationsystem.payload.request.CorpRequest;
import com.example.irrigationsystem.payload.response.CorpResponse;
import com.example.irrigationsystem.services.CorpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/corps")
public class CorpController {

    @Autowired
    private CorpService corpService;

    @PostMapping
    public ResponseEntity<CorpResponse> createCorp(@RequestBody CorpRequest corpRequest) {
        CorpResponse savedCorp = corpService.createCorp(corpRequest);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedCorp);
    }

    @GetMapping("/{corpId}")
    public ResponseEntity<CorpResponse> getCorpById(@PathVariable String corpId) {
        CorpResponse corpResponse = corpService.getCorpById(corpId);
        return ResponseEntity.ok(corpResponse);
    }

    @GetMapping
    public ResponseEntity<List<CorpResponse>> getAllCorps() {
        List<CorpResponse> corpResponses = corpService.getAllCorps();
        return ResponseEntity.ok(corpResponses);
    }

    @PutMapping("/{corpId}")
    public ResponseEntity<CorpResponse> updateCorp(@PathVariable String corpId, @RequestBody CorpRequest corpRequest) {
        CorpResponse updatedCorp = corpService.updateCorp(corpId, corpRequest);
        return ResponseEntity.ok(updatedCorp);
    }

    @DeleteMapping("/{corpId}")
    public ResponseEntity<Void> deleteCorpById(@PathVariable String corpId) {
        corpService.deleteCorpById(corpId);
        return ResponseEntity.noContent().build();
    }
}
