package com.example.irrigationsystem.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/logs")
public class LogController {

    // You can autowire any service or repository needed here

    @GetMapping
    public List<String> getLogs() {
        // Logic to retrieve logs
        // Example: Read logs from a log file
        List<String> logs = new ArrayList<>();
        try {
            Path path = Paths.get("myapp.log"); // Adjust file path as needed
            logs = Files.readAllLines(path);
        } catch (IOException e) {
            // Handle file IO error
            e.printStackTrace();
        }
        return logs;
    }
}
