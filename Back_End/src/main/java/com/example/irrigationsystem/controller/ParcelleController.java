package com.example.irrigationsystem.controller;

import com.example.irrigationsystem.model.Corp;
import com.example.irrigationsystem.model.Parcelle;
import com.example.irrigationsystem.payload.request.ParcelleRequest;
import com.example.irrigationsystem.payload.response.ParcelleResponse;
import com.example.irrigationsystem.repo.CorpRepository;
import com.example.irrigationsystem.services.ParcelleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/parcelles")
public class ParcelleController {

    @Autowired
    private ParcelleService parcelleService;

    @PostMapping
    public ResponseEntity<ParcelleResponse> createParcelle(@RequestBody ParcelleRequest parcelleRequest) {
        ParcelleResponse createdParcelle = parcelleService.createParcelle(parcelleRequest);
        return new ResponseEntity<>(createdParcelle, HttpStatus.CREATED);
    }

    @GetMapping("/{parcelleId}")
    public ResponseEntity<ParcelleResponse> getParcelleById(@PathVariable String parcelleId) {
        ParcelleResponse parcelleResponse = parcelleService.getParcelleById(parcelleId);
        return new ResponseEntity<>(parcelleResponse, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<ParcelleResponse>> getAllParcelles() {
        List<ParcelleResponse> parcelleResponses = parcelleService.getAllParcelles();
        return new ResponseEntity<>(parcelleResponses, HttpStatus.OK);
    }

    @PutMapping("/{parcelleId}")
    public ResponseEntity<ParcelleResponse> updateParcelle(@PathVariable String parcelleId, @RequestBody ParcelleRequest parcelleRequest) {
        ParcelleResponse updatedParcelle = parcelleService.updateParcelle(parcelleId, parcelleRequest);
        return new ResponseEntity<>(updatedParcelle, HttpStatus.OK);
    }

    @DeleteMapping("/{parcelleId}")
    public ResponseEntity<Void> deleteParcelleById(@PathVariable String parcelleId) {
        parcelleService.deleteParcelleById(parcelleId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
