package com.example.irrigationsystem.controller;

import com.example.irrigationsystem.model.Plant;
import com.example.irrigationsystem.payload.request.PlantRequest;
import com.example.irrigationsystem.payload.response.PlantResponse;
import com.example.irrigationsystem.services.PlantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/plants")
public class PlantController {

    @Autowired
    private PlantService plantService;

    @PostMapping
    public ResponseEntity<PlantResponse> createPlant(@RequestBody PlantRequest plantRequest) {
        PlantResponse savedPlant = plantService.createPlant(plantRequest);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedPlant);
    }

    @GetMapping("/{plantId}")
    public ResponseEntity<PlantResponse> getPlantById(@PathVariable String plantId) {
        PlantResponse plantResponse = plantService.getPlantById(plantId);
        return ResponseEntity.ok(plantResponse);
    }

    @GetMapping
    public ResponseEntity<List<PlantResponse>> getAllPlants() {
        List<PlantResponse> plantResponses = plantService.getAllPlants();
        return ResponseEntity.ok(plantResponses);
    }

    @PutMapping("/{plantId}")
    public ResponseEntity<PlantResponse> updatePlant(@PathVariable String plantId, @RequestBody PlantRequest plantRequest) {
        PlantResponse updatedPlant = plantService.updatePlant(plantId, plantRequest);
        return ResponseEntity.ok(updatedPlant);
    }

    @DeleteMapping("/{plantId}")
    public ResponseEntity<Void> deletePlantById(@PathVariable String plantId) {
        plantService.deletePlantById(plantId);
        return ResponseEntity.noContent().build();
    }
}
