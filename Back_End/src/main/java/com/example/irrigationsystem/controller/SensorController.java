package com.example.irrigationsystem.controller;

import com.example.irrigationsystem.payload.response.SensorDataResponse;
import com.example.irrigationsystem.services.SensorDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/sensor-data")
public class SensorController {

    @Autowired
    private SensorDataService sensorDataService;

    @GetMapping("/{userName}/{corpName}/{parcelName}")
    public List<SensorDataResponse> getSensorDataByUserCorpAndParcel(
            @PathVariable String userName,
            @PathVariable String corpName,
            @PathVariable String parcelName) {
        return sensorDataService.getSensorDataByUserCorpAndParcel(userName, corpName, parcelName);
    }
}
