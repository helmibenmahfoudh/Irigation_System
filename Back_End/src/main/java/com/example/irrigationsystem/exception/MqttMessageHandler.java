package com.example.irrigationsystem.exception;

import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.stereotype.Component;

@Component
public class MqttMessageHandler implements IMqttMessageListener {

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        // Process the received MQTT message here
        String payload = new String(message.getPayload());
        System.out.println("Received message on topic " + topic + ": " + payload);
        // You can handle the message payload based on your application's requirements
    }
}