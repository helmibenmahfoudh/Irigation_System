package com.example.irrigationsystem.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Document(collection = "corps")
    public class Corp {
        @Id
        private String id;
        private String name;
        private double latitude;
        private double longitude;
        private double totalSize;
        @DBRef
        @JsonIgnore
        private List<Parcelle> parcelles = new ArrayList<>(); // Initialize list here

        @DBRef
        private User owner;

        public void addParcelle(Parcelle parcelle) {
            this.parcelles.add(parcelle);
            updateTotalSize(); // Update total size when adding a new parcelle
        }

        public void removeParcelle(Parcelle parcelle) {
            this.parcelles.remove(parcelle);
            updateTotalSize(); // Update total size when removing a parcelle
        }

        public void updateTotalSize() {
            totalSize = parcelles.stream()
                    .filter(Objects::nonNull) // Filter out null elements
                    .mapToDouble(Parcelle::getSize)
                    .sum();
        }

        // Method to set the owner and ensure the owner's corp list is updated

    }
