package com.example.irrigationsystem.model;

public enum ERole {

    ADMIN,
    FARMER,
    EMPLOYEE

}
