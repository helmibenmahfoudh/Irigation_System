package com.example.irrigationsystem.model;

public enum ESoilType {

    SANDY_LOAM,  // Well-drained, good for various plants
    CLAY,                // Retains water well, suitable for moisture-loving plants
    LOAM,                // Balanced mix, ideal for most plants
    SANDY,               // Drains very quickly, suitable for drought-tolerant plants
    SILT,                 // Holds moisture and nutrients, good for some vegetables
    PEAT_MOSS,      // Highly acidic, ideal for acid-loving plants
    CHALK               // Alkaline, suitable for plants preferring higher pH
    ;
}
