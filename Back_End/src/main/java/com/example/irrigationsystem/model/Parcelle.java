package com.example.irrigationsystem.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Optional;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "parcelle")
public class Parcelle {

    @Id
    private String id;
    private String name;
    private ESoilType soilType;
    private double size;
    private Plant plant;
    @DBRef
    private Corp corp;

    public void setCorp(Optional<Corp> optionalCorp) {
        Corp corp = optionalCorp.orElse(null);
        this.corp = corp;
        if (corp != null) {
            corp.addParcelle(this); // Add this parcelle to the Corp's list
        }
    }
}

