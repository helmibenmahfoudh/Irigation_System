package com.example.irrigationsystem.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "sensor_data")
public class SensorData {

    @Id
    private String id;
    private String sensorName;
    private String data;
    private LocalDateTime timestamp;
    private String parcelName;
    private String corpName ;
    private String userName;

}
