package com.example.irrigationsystem.payload.request;


import com.example.irrigationsystem.payload.response.UserResponse;
import lombok.Data;

@Data
public class CorpRequest {

    private String name;
    private double latitude;
    private double longitude;
    private UserResponse owner;


}
