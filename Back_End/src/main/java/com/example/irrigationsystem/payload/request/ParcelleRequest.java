package com.example.irrigationsystem.payload.request;

import com.example.irrigationsystem.model.ESoilType;
import com.example.irrigationsystem.payload.response.CorpResponse;
import com.example.irrigationsystem.payload.response.PlantResponse;
import lombok.Data;


@Data
public class ParcelleRequest {

    private String name;
    private ESoilType soilType;
    private double size;
    private CorpResponse corp;
    private PlantResponse plant;
}
