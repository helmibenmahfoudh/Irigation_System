package com.example.irrigationsystem.payload.request;

import lombok.Data;

@Data
public class PlantRequest {

    private String name;
}
