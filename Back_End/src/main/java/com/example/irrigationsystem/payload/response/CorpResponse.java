package com.example.irrigationsystem.payload.response;

import com.example.irrigationsystem.model.Parcelle;
import com.example.irrigationsystem.repo.ParcelleRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Component
public class CorpResponse {

    private String id;
    private String name;
    private double latitude;
    private double longitude;
    private List<Parcelle> parcelleList;
    private double totalSize;
    private UserResponse owner ;
    public CorpResponse(String id, String name, Double latitude,Double longitude, ParcelleRepository parcelleRepository) {
        this.id = id;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.parcelleList = parcelleRepository.getParcelleByCorpId(id);
        this.totalSize = parcelleList.stream().mapToDouble(Parcelle::getSize).sum();
    }

    // ... other methods
}