package com.example.irrigationsystem.payload.response;


import com.example.irrigationsystem.model.ESoilType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ParcelleResponse {
    private String id;
    private String name;
    private ESoilType soilType;
    private double size;
    private PlantResponse plant;
    private CorpResponse corp ;

}
