package com.example.irrigationsystem.payload.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SensorDataResponse {

    private String id;
    private String sensorName;
    private String data;
    private LocalDateTime timestamp;
}
