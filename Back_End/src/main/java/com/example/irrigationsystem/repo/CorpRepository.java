package com.example.irrigationsystem.repo;


import com.example.irrigationsystem.model.Corp;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface CorpRepository  extends MongoRepository<Corp, String> {

}
