package com.example.irrigationsystem.repo;

import com.example.irrigationsystem.model.Corp;
import com.example.irrigationsystem.model.Parcelle;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;



public interface ParcelleRepository  extends MongoRepository<Parcelle, String> {

    List<Parcelle> getParcelleByCorpId(String id) ;

}
