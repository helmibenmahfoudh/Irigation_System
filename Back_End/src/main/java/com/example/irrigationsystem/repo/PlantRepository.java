package com.example.irrigationsystem.repo;

import com.example.irrigationsystem.model.Plant;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PlantRepository  extends MongoRepository<Plant, String> {
}
