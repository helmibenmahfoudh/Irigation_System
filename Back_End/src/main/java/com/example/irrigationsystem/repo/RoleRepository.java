package com.example.irrigationsystem.repo;

import com.example.irrigationsystem.model.ERole;
import com.example.irrigationsystem.model.Role;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Set;

public interface RoleRepository extends MongoRepository<Role, String> {
    Set<Role> findByName(ERole name);
}
