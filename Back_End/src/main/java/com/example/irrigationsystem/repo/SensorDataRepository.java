package com.example.irrigationsystem.repo;

import com.example.irrigationsystem.model.SensorData;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SensorDataRepository extends MongoRepository<SensorData, String> {
    List<SensorData> findByUserNameAndCorpNameAndParcelName(String userName, String corpName, String parcelName);
}