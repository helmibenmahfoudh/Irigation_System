package com.example.irrigationsystem.services;

import com.example.irrigationsystem.exception.ResourceNotFoundException;
import com.example.irrigationsystem.model.Corp;
import com.example.irrigationsystem.model.Parcelle;
import com.example.irrigationsystem.model.User;
import com.example.irrigationsystem.payload.request.CorpRequest;
import com.example.irrigationsystem.payload.response.CorpResponse;
import com.example.irrigationsystem.payload.response.UserResponse;
import com.example.irrigationsystem.repo.CorpRepository;
import com.example.irrigationsystem.repo.ParcelleRepository;
import com.example.irrigationsystem.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CorpService {

    @Autowired
    private CorpRepository corpRepository;

    @Autowired
    private  UserRepository userRepository ;

    @Autowired
    private ParcelleRepository parcelleRepository;


    public CorpResponse createCorp(CorpRequest corpRequest) {
        // Retrieve user information from the CorpRequest
        UserResponse owner = corpRequest.getOwner();
        User user = userRepository.findById(owner.getId())
                .orElseThrow(() -> new ResourceNotFoundException("Owner not found"));

        Corp corp = new Corp();
        corp.setName(corpRequest.getName());
        corp.setLatitude(corpRequest.getLatitude());
        corp.setLongitude(corpRequest.getLongitude());
        corp.setOwner(user); // Set the owner for the corporation

        Corp savedCorp = corpRepository.save(corp);

        CorpResponse corpResponse = new CorpResponse();
        corpResponse.setId(savedCorp.getId());
        corpResponse.setName(savedCorp.getName());
        corpResponse.setLatitude(savedCorp.getLatitude());
        corpResponse.setLongitude(savedCorp.getLongitude());
        corpResponse.setOwner(new UserResponse(corp.getOwner().getId(),corp.getOwner().getUsername(),corp.getOwner().getEmail())); // Set the owner in the response
        // Set other fields as needed

        return corpResponse;
    }

    public CorpResponse getCorpById(String id) {
        Corp corp = corpRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Corp not found with id: " + id));

        CorpResponse corpResponse = new CorpResponse();
        corpResponse.setId(corp.getId());
        corpResponse.setName(corp.getName());
        corpResponse.setLatitude(corp.getLatitude());
        corpResponse.setLongitude(corp.getLongitude());
        List<Parcelle> parcelles = parcelleRepository.getParcelleByCorpId(corp.getId());
        corpResponse.setParcelleList(parcelles);
        corpResponse.setTotalSize(corp.getTotalSize());
        corpResponse.setOwner(new UserResponse(corp.getOwner().getId(),corp.getOwner().getUsername(),corp.getOwner().getEmail()));
        return corpResponse;
    }

    public List<CorpResponse> getAllCorps() {
        List<Corp> corps = corpRepository.findAll();
        List<CorpResponse> corpResponses = new ArrayList<>();
        for (Corp corp : corps) {
            CorpResponse corpResponse = new CorpResponse();
            corpResponse.setId(corp.getId());
            corpResponse.setName(corp.getName());
            corpResponse.setLatitude(corp.getLatitude());
            corpResponse.setLongitude(corp.getLongitude());
            corpResponse.setTotalSize(corp.getTotalSize());
            corpResponse.setOwner(new UserResponse(corp.getOwner().getId(),corp.getOwner().getUsername(),corp.getOwner().getEmail()));

            // Populate parcelleList

            List<Parcelle> parcelles = parcelleRepository.getParcelleByCorpId(corp.getId());
            corpResponse.setParcelleList(parcelles);
            corpResponses.add(corpResponse);

        }
        return corpResponses;
    }

    public CorpResponse updateCorp(String id, CorpRequest corpRequest) {
        Corp corp = corpRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Corp not found with id: " + id));
        UserResponse owner = corpRequest.getOwner();
        User user = userRepository.findById(owner.getId())
                .orElseThrow(() -> new ResourceNotFoundException("Owner not found"));
        corp.setName(corpRequest.getName());
        corp.setLatitude(corpRequest.getLatitude());
        corp.setLongitude(corpRequest.getLongitude());
        corp.setOwner(user);
        Corp updatedCorp = corpRepository.save(corp);

        CorpResponse corpResponse = new CorpResponse();
        corpResponse.setId(updatedCorp.getId());
        corpResponse.setName(updatedCorp.getName());
        corpResponse.setLatitude(updatedCorp.getLatitude());
        corpResponse.setLongitude(updatedCorp.getLongitude());
        corpResponse.setOwner(new UserResponse(corp.getOwner().getId(),corp.getOwner().getUsername(),corp.getOwner().getEmail()));
        return corpResponse;
    }

    public void deleteCorpById(String id) {
        // Retrieve the corporation by its ID
        Corp corp = corpRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Corp not found with id: " + id));

        // Get the list of associated parcels
        List<Parcelle> parcelles = corp.getParcelles();

        // Delete each parcel individually
        for (Parcelle parcelle : parcelles) {
            parcelleRepository.deleteById(parcelle.getId());
        }

        // Delete the corporation itself
        corpRepository.deleteById(id);
    }
}
