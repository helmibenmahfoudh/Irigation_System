package com.example.irrigationsystem.services;

import com.example.irrigationsystem.exception.ResourceNotFoundException;
import com.example.irrigationsystem.model.Corp;
import com.example.irrigationsystem.model.Parcelle;
import com.example.irrigationsystem.model.Plant;
import com.example.irrigationsystem.payload.request.ParcelleRequest;
import com.example.irrigationsystem.payload.response.CorpResponse;
import com.example.irrigationsystem.payload.response.ParcelleResponse;
import com.example.irrigationsystem.payload.response.PlantResponse;
import com.example.irrigationsystem.repo.CorpRepository;
import com.example.irrigationsystem.repo.ParcelleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ParcelleService {

    @Autowired
    private ParcelleRepository parcelleRepository;

    @Autowired
    private CorpRepository corpRepository;

    public ParcelleResponse createParcelle(ParcelleRequest parcelleRequest) {
        Parcelle parcelle = new Parcelle();
        parcelle.setName(parcelleRequest.getName());
        parcelle.setSoilType(parcelleRequest.getSoilType());
        parcelle.setSize(parcelleRequest.getSize());

        Corp corp = corpRepository.findById(parcelleRequest.getCorp().getId())
                .orElseThrow(() -> new ResourceNotFoundException("Corp not found"));

        parcelle.setCorp(Optional.ofNullable(corp));

        Plant plant = new Plant();
        plant.setName(parcelleRequest.getPlant().getName());
        parcelle.setPlant(plant);



        Parcelle savedParcelle = parcelleRepository.save(parcelle);
//        // Add the new parcelle to the corporation's list of parcelles
//        assert corp != null;
//        corp.addParcelle(parcelle);

        // Save the updated corporation with the new parcelle
        corpRepository.save(corp);

        ParcelleResponse parcelleResponse = new ParcelleResponse();
        parcelleResponse.setId(savedParcelle.getId());
        parcelleResponse.setName(savedParcelle.getName());
        parcelleResponse.setSoilType(savedParcelle.getSoilType());
        parcelleResponse.setSize(savedParcelle.getSize());
        parcelleResponse.setPlant(new PlantResponse(plant.getId(), plant.getName()));
        parcelleResponse.setCorp(new CorpResponse(corp.getId(), corp.getName(), corp.getLatitude(),corp.getLongitude(),parcelleRepository));
        return parcelleResponse;
    }

    public ParcelleResponse getParcelleById(String parcelleId) {
        Parcelle parcelle = parcelleRepository.findById(parcelleId)
                .orElseThrow(() -> new ResourceNotFoundException("Parcelle not found with id: " + parcelleId));

        ParcelleResponse parcelleResponse = new ParcelleResponse();
        parcelleResponse.setId(parcelle.getId());
        parcelleResponse.setName(parcelle.getName());
        parcelleResponse.setSoilType(parcelle.getSoilType());
        parcelleResponse.setSize(parcelle.getSize());
        parcelleResponse.setPlant(new PlantResponse(parcelle.getPlant().getId(), parcelle.getPlant().getName()));
        parcelleResponse.setCorp(new CorpResponse(parcelle.getCorp().getId(), parcelle.getCorp().getName(), parcelle.getCorp().getLatitude(),parcelle.getCorp().getLongitude(),parcelleRepository));
        return parcelleResponse;
    }

    public List<ParcelleResponse> getAllParcelles() {
        List<Parcelle> parcelles = parcelleRepository.findAll();
        List<ParcelleResponse> parcelleResponses = new ArrayList<>();
        for (Parcelle parcelle : parcelles) {
            ParcelleResponse parcelleResponse = new ParcelleResponse();
            parcelleResponse.setId(parcelle.getId());
            parcelleResponse.setName(parcelle.getName());
            parcelleResponse.setSoilType(parcelle.getSoilType());
            parcelleResponse.setSize(parcelle.getSize());
            parcelleResponse.setPlant(new PlantResponse(parcelle.getPlant().getId(), parcelle.getPlant().getName()));
            parcelleResponse.setCorp(new CorpResponse(parcelle.getCorp().getId(), parcelle.getCorp().getName(),parcelle.getCorp().getLatitude(),parcelle.getCorp().getLongitude(),parcelleRepository));
            parcelleResponses.add(parcelleResponse);
        }
        return parcelleResponses;
    }

    public ParcelleResponse updateParcelle(String parcelleId, ParcelleRequest parcelleRequest) {
        Parcelle parcelle = parcelleRepository.findById(parcelleId)
                .orElseThrow(() -> new ResourceNotFoundException("Parcelle not found with id: " + parcelleId));

        parcelle.setName(parcelleRequest.getName());
        parcelle.setSoilType(parcelleRequest.getSoilType());
        parcelle.setSize(parcelleRequest.getSize());

        Corp corp = corpRepository.findById(parcelleRequest.getCorp().getId())
                .orElseThrow(() -> new ResourceNotFoundException("Corp not found"));

        parcelle.setCorp(Optional.ofNullable(corp));

        // Fetch existing Plant if ID provided, else create a new one
        if (parcelleRequest.getPlant().getId() != null) {
            Plant plant = new Plant();
            plant.setId(parcelleRequest.getPlant().getId());
            parcelle.setPlant(plant);
        } else {
            Plant plant = new Plant();
            plant.setName(parcelleRequest.getPlant().getName());
            parcelle.setPlant(plant);
        }


        Parcelle updatedParcelle = parcelleRepository.save(parcelle);


        ParcelleResponse parcelleResponse = new ParcelleResponse();
        parcelleResponse.setId(updatedParcelle.getId());
        parcelleResponse.setName(updatedParcelle.getName());
        parcelleResponse.setSoilType(updatedParcelle.getSoilType());
        parcelleResponse.setSize(updatedParcelle.getSize());
        parcelleResponse.setPlant(new PlantResponse(updatedParcelle.getPlant().getId(), updatedParcelle.getPlant().getName()));
        parcelleResponse.setCorp(new CorpResponse(updatedParcelle.getCorp().getId(), updatedParcelle.getCorp().getName(), updatedParcelle.getCorp().getLatitude(),updatedParcelle.getCorp().getLongitude(),parcelleRepository));
        return parcelleResponse;
    }

    public void deleteParcelleById(String parcelleId) {
        Parcelle parcelle = parcelleRepository.findById(parcelleId)
                .orElseThrow(() -> new ResourceNotFoundException("Parcelle not found with id: " + parcelleId));

        Corp corp = parcelle.getCorp();
        if (corp != null) {
            // Subtract the size of the deleted parcelle from the corporation's total size
            corp.setTotalSize(corp.getTotalSize() - parcelle.getSize());
            // Remove the deleted parcelle from the list of parcelles associated with the corporation
            corp.getParcelles().remove(parcelle);
            corpRepository.save(corp);
        }

        parcelleRepository.deleteById(parcelleId);
    }
}
