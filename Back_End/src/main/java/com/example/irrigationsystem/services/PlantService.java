package com.example.irrigationsystem.services;


import com.example.irrigationsystem.exception.ResourceNotFoundException;
import com.example.irrigationsystem.model.Corp;
import com.example.irrigationsystem.model.Parcelle;
import com.example.irrigationsystem.model.Plant;
import com.example.irrigationsystem.payload.request.PlantRequest;
import com.example.irrigationsystem.payload.response.CorpResponse;
import com.example.irrigationsystem.payload.response.PlantResponse;
import com.example.irrigationsystem.repo.CorpRepository;
import com.example.irrigationsystem.repo.ParcelleRepository;
import com.example.irrigationsystem.repo.PlantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PlantService {

    @Autowired
    private PlantRepository plantRepository;

    public PlantResponse createPlant(PlantRequest plantRequest) {
        Plant plant = new Plant();
        plant.setName(plantRequest.getName());
        Plant savedPlant = plantRepository.save(plant);

        PlantResponse plantResponse = new PlantResponse();
        plantResponse.setId(savedPlant.getId());
        plantResponse.setName(savedPlant.getName());
        return plantResponse;
    }

    public PlantResponse getPlantById(String plantId) {
        Plant plant = plantRepository.findById(plantId)
                .orElseThrow(() -> new ResourceNotFoundException("Plant not found with id: " + plantId));

        PlantResponse plantResponse = new PlantResponse();
        plantResponse.setId(plant.getId());
        plantResponse.setName(plant.getName());
        return plantResponse;
    }

    public List<PlantResponse> getAllPlants() {
        List<Plant> plants = plantRepository.findAll();
        return plants.stream()
                .map(plant -> {
                    PlantResponse plantResponse = new PlantResponse();
                    plantResponse.setId(plant.getId());
                    plantResponse.setName(plant.getName());
                    return plantResponse;
                })
                .collect(Collectors.toList());
    }

    public PlantResponse updatePlant(String plantId, PlantRequest plantRequest) {
        Plant plant = plantRepository.findById(plantId)
                .orElseThrow(() -> new ResourceNotFoundException("Plant not found with id: " + plantId));
        plant.setName(plantRequest.getName());
        Plant updatedPlant = plantRepository.save(plant);

        PlantResponse plantResponse = new PlantResponse();
        plantResponse.setId(updatedPlant.getId());
        plantResponse.setName(updatedPlant.getName());
        return plantResponse;
    }

    public void deletePlantById(String plantId) {
        plantRepository.deleteById(plantId);
    }
}
