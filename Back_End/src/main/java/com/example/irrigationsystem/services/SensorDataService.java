package com.example.irrigationsystem.services;


import com.example.irrigationsystem.model.SensorData;
import com.example.irrigationsystem.payload.response.SensorDataResponse;
import com.example.irrigationsystem.repo.SensorDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SensorDataService {

    @Autowired
    private SensorDataRepository sensorDataRepository;

    public List<SensorDataResponse> getSensorDataByUserCorpAndParcel(String userName, String corpName, String parcelName) {
        // Query sensor data by user name, corp name, and parcel name
        List<SensorData> sensorDataList = sensorDataRepository.findByUserNameAndCorpNameAndParcelName(userName, corpName, parcelName);

        // Map sensor data to response objects
        List<SensorDataResponse> sensorDataResponseList = new ArrayList<>();
        for (SensorData sensorData : sensorDataList) {
            SensorDataResponse response = new SensorDataResponse(
                    sensorData.getId(),
                    sensorData.getSensorName(),
                    sensorData.getData(),
                    sensorData.getTimestamp()
            );
            sensorDataResponseList.add(response);
        }
        return sensorDataResponseList;
    }
}