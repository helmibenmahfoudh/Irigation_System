package com.example.irrigationsystem.services;

import com.example.irrigationsystem.model.User;
import com.example.irrigationsystem.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<User> getAllUsers(){
        return userRepository.findAll();
    }

    public Optional<User> getUserById(String id){
        return userRepository.findById(id);
    }

    public void updateUser(User user){
        userRepository.save(user);
    }

    public void deleteUserById(String id){
        userRepository.deleteById(id);
    }



}
