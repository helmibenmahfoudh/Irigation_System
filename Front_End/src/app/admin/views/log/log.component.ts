import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { LogService } from 'src/app/services/log/log.service';

@Component({
  selector: 'app-log',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './log.component.html',
  styleUrl: './log.component.scss'
})
export class LogComponent implements OnInit {
  logs: string[] = [];

  constructor(private logService: LogService) {}

  ngOnInit(): void {
    this.getLogData();
  }

  getLogData(): void {
    this.logService.getLogs()
      .subscribe(
        logs => {
          this.logs = logs;
          console.log(logs)
        },
        error => {
          console.error('Error fetching logs:', error);
        }
      );
  }
}