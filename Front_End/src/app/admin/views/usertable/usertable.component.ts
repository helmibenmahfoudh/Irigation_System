import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/model/user';
import { UserserviceService } from 'src/app/services/user/userservice.service';
import { ToastModule } from 'primeng/toast';
import { CommonModule } from '@angular/common';
import { ToolbarModule } from 'primeng/toolbar';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { MessageModule } from 'primeng/message';
import { Header, MessageService } from 'primeng/api';
import { FileUploadModule } from 'primeng/fileupload';
import { ButtonModule } from 'primeng/button';
import { RippleModule } from 'primeng/ripple';
import { RatingModule } from 'primeng/rating';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { DropdownModule } from 'primeng/dropdown';
import { RadioButtonModule } from 'primeng/radiobutton';
import { InputNumberModule } from 'primeng/inputnumber';
import { AuthService } from 'src/app/services/auth/auth.service';
import * as _ from 'lodash';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { Corp } from 'src/app/model/corp';
import { CorpService } from 'src/app/services/corp/corp.service';

@Component({
  selector: 'app-usertable',
  standalone: true,
  imports: [CommonModule,ToastModule,ToolbarModule,TableModule,DialogModule,MessageModule,FileUploadModule,ButtonModule,RippleModule,RatingModule,InputNumberModule,InputTextModule,InputTextareaModule,DropdownModule,RadioButtonModule,FormsModule,RouterModule],
  templateUrl: './usertable.component.html',
  styleUrl: './usertable.component.scss',
  providers: [MessageService]
})
export class UsertableComponent implements OnInit {
  users: User[] = [];
  selectedUsers: User[] = [];
  corps: Corp[] = [];
  newUser: User = {
    username: '',
    email: '',
    password: '',
    firstName: '',
    lastName: '',
    roles: []
  };
  userDialog: boolean = false;
  deleteUserDialog: boolean = false;
  submitted: boolean = false;
  isFarmer : Boolean ;
  isAdmin : Boolean ;
  currentUser : User
  cols: any[] = [
    { field: 'username', header: 'Username' },
    { field: 'email', header: 'Email' },
    { field: 'firstName', header: 'First Name' },
    { field: 'lastName', header: 'Last Name' }
  ];
  expandedRows: any = {};

  constructor(private userService: UserserviceService, private messageService: MessageService, private authService: AuthService, private corpService : CorpService) {}

  ngOnInit() {
    this.currentUser = this.authService.getCurrentUser();
    if (this.currentUser) {
      this.isAdmin = this.currentUser.roles.includes('ADMIN');
      this.isFarmer = this.currentUser.roles.includes('FARMER');
    }
    this.loadUsers();
    this.loadCorps();
  }

  loadUsers() {
    this.userService.getUsers().subscribe(
      (users: User[]) => {
        if (this.isAdmin) {
          this.users = users.filter(userItem => userItem.roles.some((role: any) => role.name === 'FARMER'));
        } else if (this.isFarmer) {
          this.users = users.filter(user => user.createdBy && user.createdBy === this.currentUser.username);
        }

        console.log('Filtered Users:', this.users);

        // Load corporation data for each user
        this.corpService.getAllCorps().subscribe(
          (corps: Corp[]) => {
            this.corps = corps;
            console.log('All Corps:', this.corps);

            this.users.forEach(user => {
              const userCorp = corps.find(corp => corp.owner.username === user.username);
              if (userCorp) {
                user.corp = userCorp;
              }
              console.log('User after adding corp:', user.corp);
            });
          },
          (error) => {
            console.error('Error fetching corporation data:', error);
          }
        );
      },
      (error) => {
        console.error('Error fetching users:', error);
      }
    );
  }
  loadCorps() {
    this.corpService.getAllCorps().subscribe(corps => {
      this.corps = corps;
      console.log(this.corps); // Move the logging here
    });
  }

  openNew() {
    this.newUser = { username: '', email: '', password: '', firstName: '', lastName: '', roles: [] };
    this.submitted = false;
    this.userDialog = true;
  }

  deleteSelectedUsers() {
    this.deleteUserDialog = true;
  }

  saveUser() {
    this.submitted = true;
    if (this.newUser.username && this.newUser.email && this.newUser.password) {
      this.authService.signup(this.newUser.username, this.newUser.email, this.newUser.password, this.newUser.firstName, this.newUser.lastName).subscribe(
        (success) => {
          if (success) {
            this.loadUsers();
            this.userDialog = false;
            this.messageService.add({ severity: 'success', summary: 'Success', detail: 'User created successfully' });
          } else {
            this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Failed to create user' });
          }
        },
        (error) => {
          console.error('Error creating user:', error);
          this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Failed to create user' });
        }
      );
    }
  }

  editUser(user: User) {
    this.newUser = { ...user };
    this.userDialog = true;
  }

  deleteUser(user: User) {
    this.deleteUserDialog = true;
    this.newUser = { ...user };
  }

  updateUser() {
    this.userService.updateUser(this.newUser.id!, this.newUser).subscribe(
      () => {
        this.loadUsers();
        this.userDialog = false;
        this.messageService.add({ severity: 'success', summary: 'Success', detail: 'User updated successfully' });
      },
      (error) => {
        console.error('Error updating user:', error);
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Failed to update user' });
      }
    );
  }

  confirmDelete() {
    this.deleteUserDialog = false;
    this.userService.deleteUser(this.newUser.id!).subscribe(() => {
      this.loadUsers();
    });
  }

  hideDialog() {
    this.userDialog = false;
    this.deleteUserDialog = false;
  }

  onGlobalFilter(dt: any, event: Event) {
    if (dt && event) {
      const filterValue = (event.target as HTMLInputElement).value;
      dt.filterGlobal(filterValue, 'contains');
    }
  }
}