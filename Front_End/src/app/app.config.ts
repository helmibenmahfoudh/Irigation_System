import { ApplicationConfig } from '@angular/core';
import { provideRouter } from '@angular/router';

import { routes } from './app.routes';
import { provideHttpClient, withFetch, withInterceptors } from '@angular/common/http';
import { provideClientHydration } from '@angular/platform-browser';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { AuthGuard } from './services/guards/auth.guard';
import { authInterceptorInterceptor } from './services/interceptors/auth-interceptor.interceptor';

export const appConfig: ApplicationConfig = {
  providers: [ provideRouter(routes), 
    provideClientHydration(),
    provideHttpClient( withInterceptors([authInterceptorInterceptor])), 
    provideHttpClient(withFetch()),
    provideAnimationsAsync(), 
    AuthGuard,]
};
