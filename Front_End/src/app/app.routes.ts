import { Routes } from '@angular/router';
import { LoginComponent } from './components/auth/login/login.component';
import { RegisterComponent } from './components/auth/register/register.component';
import { DashboardComponent } from './admin/views/dashboard/dashboard.component';
import { AuthGuard } from './services/guards/auth.guard';
import { UsertableComponent } from './admin/views/usertable/usertable.component';
import { LogComponent } from './admin/views/log/log.component';
import { UnauthorizedComponent } from './components/unauthorized/unauthorized.component';
import { CorptableComponent } from './components/corp/corptable/corptable.component';
import { ParcelleComponent } from './components/parcelle/parcelle/parcelle.component';
import { PlantComponent } from './components/plant/plant.component';
import { ParceldashboardComponent } from './components/parcelle/parceldashboard/parceldashboard.component';
import { CorpdashboardComponent } from './components/corp/corpdashboard/corpdashboard.component';



export const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'unauthorized', component: UnauthorizedComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'parceldashboard/:corpName/:parcelName', component: ParceldashboardComponent },
  { path: 'corpdashboard/:id/:latitude/:longitude', component: CorpdashboardComponent },
  { path: 'users', component: UsertableComponent },
  {
    path: 'admin',
    canActivate: [AuthGuard],
    data: { roles: ['ADMIN'] },
    //component: AdminLayoutComponent, // Use AdminLayoutComponent for admin routes
    children: [
      { path: '', component: UsertableComponent }, // Default child route for admin
      { path: 'logs', component: LogComponent },
      // Add more child routes for admin as needed
    ]
  },
  {
    path: 'farmer',
    canActivate: [AuthGuard],
    data: { roles: ['FARMER'] },
    //component: FarmerLayoutComponent, // Use FarmerLayoutComponent for farmer routes
    children: [
      { path: '', component: CorptableComponent }, // Default child route for farmer
      { path: 'plants', component: PlantComponent },
      { path: 'parcelles', component: ParcelleComponent },
      // Add more child routes for farmer as needed
    ]
  },
  // ... other routes
];