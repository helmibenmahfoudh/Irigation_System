import { Component, OnInit } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { FormBuilder, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import {PasswordModule} from 'primeng/password';
import {CheckboxModule} from 'primeng/checkbox'
import { ButtonModule } from 'primeng/button';


@Component({
  selector: 'app-login',
  standalone: true,
  imports: [PasswordModule,FormsModule,CheckboxModule,RouterModule,ReactiveFormsModule,ButtonModule],
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent implements OnInit {

  constructor(private authService : AuthService,
     private fb : FormBuilder, 
     private router: Router, 
     private snackBar: MatSnackBar) {}

  loginForm:any

  
  ngOnInit(): void {
    this.loginForm = this.fb.group({
      username : ['', [Validators.required]],
      password : ['', Validators.required],
      remember: ['']
    })
  }

  signin() {
    if (this.loginForm.valid) {
      let remember = this.loginForm.value.remember ? true : false;
      this.authService.login(this.loginForm.value.username, this.loginForm.value.password, remember).subscribe(
        (resp) => {
          if (resp) {
            const roles = this.authService.getUserRoles(); // Assuming getUserRoles() returns an array
            if (roles.length > 0) {
              roles.forEach(role => {
                console.log(role);
                // Navigate based on the role
                this.router.navigate([`/${role.toLowerCase()}`]); // Assuming role is 'ADMIN' or similar
              });
            } else {
              // Handle scenario when no roles are available
              console.error("No roles found for the user.");
            }
          } else {
            this.snackBar.open("Please verify your credentials", "OK", { horizontalPosition: "right", duration: 3000 });
          }
        }
      );
    } else {
      this.snackBar.open("Please verify your inputs", "OK", { horizontalPosition: "right", duration: 3000 });
    }
  }
}

