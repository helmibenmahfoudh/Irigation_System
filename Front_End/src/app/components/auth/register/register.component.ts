import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from 'src/app/services/auth/auth.service';
import { MatFormFieldModule } from '@angular/material/form-field';
import { Router, RouterModule } from '@angular/router';
import { CheckboxModule } from 'primeng/checkbox';
import { PasswordModule } from 'primeng/password';

@Component({
  selector: 'app-register',
  standalone: true,
  imports: [PasswordModule,FormsModule,CheckboxModule,RouterModule,ReactiveFormsModule],
  templateUrl: './register.component.html',
  styleUrl: './register.component.scss'
})
export class RegisterComponent implements OnInit{

  registerForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      username: ['',Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
    });
  }

  signup(){
    if (this.registerForm.valid) {
      this.authService.signup(this.registerForm.value.username, this.registerForm.value.email, this.registerForm.value.password, this.registerForm.value.firstName, this.registerForm.value.lastName).subscribe(
        (success) => {
          if (success) {
            // Registration successful
            this.snackBar.open('Registration successful!', 'Close', { duration: 3000 });
            this.router.navigate([''])
            // Optionally, you can navigate to another page upon successful registration
          } else {
            // Registration failed
            this.snackBar.open('Registration failed. Please try again later.', 'Close', { duration: 3000 });
          }
        },
        (error) => {
          // Error occurred during registration
          console.error('Error during registration:', error);
          this.snackBar.open('An error occurred during registration. Please try again later.', 'Close', { duration: 3000 });
        }
      );
    } else {
      // Form is invalid
      this.snackBar.open('Please fill in all required fields correctly.', 'Close', { duration: 3000 });
    }
  }
  }
