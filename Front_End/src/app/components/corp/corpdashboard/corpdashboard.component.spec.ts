import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CorpdashboardComponent } from './corpdashboard.component';

describe('CorpdashboardComponent', () => {
  let component: CorpdashboardComponent;
  let fixture: ComponentFixture<CorpdashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CorpdashboardComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CorpdashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
