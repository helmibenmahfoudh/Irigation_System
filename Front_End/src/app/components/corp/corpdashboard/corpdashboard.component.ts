import { CommonModule } from '@angular/common';
import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { WeatherService } from 'src/app/services/weather/weather.service';
import * as L from 'leaflet';
import { Subscription } from 'rxjs';
import { ChartModule } from 'primeng/chart';
import { CorpService } from 'src/app/services/corp/corp.service';
import { Corp } from 'src/app/model/corp';
import { TableModule } from 'primeng/table';
import { AuthService } from 'src/app/services/auth/auth.service';
import { WebSocketService } from 'src/app/services/websocket/websocket.service';
import { ButtonModule } from 'primeng/button';

@Component({
  selector: 'app-corpdashboard',
  standalone: true,
  imports: [CommonModule,ChartModule,TableModule,ButtonModule,RouterModule],
  templateUrl: './corpdashboard.component.html',
  styleUrl: './corpdashboard.component.scss'
})
export class CorpdashboardComponent implements OnInit, AfterViewInit {

  @Input() latitude: any;
  @Input() longitude: any;
  @Input() id : any ;
  weatherData: any;
  precipitationData: any;
  corp: Corp ;
  chartData: any;
  chartOptions: any;

  subscription!: Subscription;
  wsSubscription!: Subscription;
  
  
  constructor(private weatherService: WeatherService, 
    private route: ActivatedRoute,
    private corpService: CorpService,
    ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      // Fetch latitude and longitude from route parameters
      this.latitude = params['latitude'];
      this.longitude = params['longitude'];
      this.id = params['id']
      // Fetch weather data using provided coordinates
      this.fetchWeatherData();
    });
    this.getCorpById(this.id);
    this.fetchPrecipitationData();
    this.initChart();
    this.initializeMap();
    
  }

  getCorpById(id: string): void {
    this.corpService.getCorpById(id).subscribe(
      (corp: Corp) => {
        this.corp = corp;
        console.log('Retrieved corp:', corp);
      },
      (error) => {
        console.error('Error retrieving corp:', error);
      }
    );
  }

  fetchWeatherData(): void {
    // Fetch weather data using provided coordinates
    this.weatherService.getWeather(this.latitude, this.longitude)
      .subscribe(data => {
        // Assign received weather data to weatherData property
        this.weatherData = data;
      });
  }

  initializeMap(): void {
    const map = L.map('map').setView([this.latitude, this.longitude], 13);
  
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; OpenStreetMap contributors'
    }).addTo(map);
  
    L.marker([this.latitude, this.longitude]).addTo(map)
      .bindPopup('Votre position')
      .openPopup();
  }
  
  fetchPrecipitationData(): void {
    this.weatherService.getPrecipitation(this.latitude, this.longitude)
      .subscribe(data => {
        this.precipitationData = data;
        // Process data and update chart
        this.updateChart();
      });
  }

  updateChart(): void {
    if (this.precipitationData && this.precipitationData.daily) {
      this.chartData = {
        labels: this.precipitationData.daily.time,
        datasets: [
          {
            label: 'Precipitation',
            data: this.precipitationData.daily.precipitation_sum,
            fill: false,
            backgroundColor: 'rgba(75,192,192,0.4)',
            borderColor: 'rgba(75,192,192,1)',
            borderWidth: 1
          }
        ]
      };
    }
  }
  

  initChart(): void {
    this.chartOptions = {
      scales: {
        x: {
          ticks: {
            color: 'red'
          }
        },
        y: {
          ticks: {
            color: 'blue'
          }
        }
      }
    };
  }

  ngAfterViewInit(): void {
    
  }

}
