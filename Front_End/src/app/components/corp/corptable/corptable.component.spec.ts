import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CorptableComponent } from './corptable.component';

describe('CorptableComponent', () => {
  let component: CorptableComponent;
  let fixture: ComponentFixture<CorptableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CorptableComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CorptableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
