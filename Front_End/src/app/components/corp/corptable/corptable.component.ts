import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Router, RouterModule } from '@angular/router';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { RippleModule } from 'primeng/ripple';
import { MultiSelectModule } from 'primeng/multiselect';
import { DropdownModule } from 'primeng/dropdown';
import { ProgressBarModule } from 'primeng/progressbar';
import { ToastModule } from 'primeng/toast';
import { SliderModule } from 'primeng/slider';
import { RatingModule } from 'primeng/rating';
import { Corp } from 'src/app/model/corp';
import { CorpService } from 'src/app/services/corp/corp.service';
import { MessageService } from 'primeng/api';
import { MessageModule } from 'primeng/message';
import { DialogModule } from 'primeng/dialog';
import { ToolbarModule } from 'primeng/toolbar';
import { UserserviceService } from 'src/app/services/user/userservice.service';
import { User } from 'src/app/model/user';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-corptable',
  standalone: true,
  imports: [CommonModule,RouterModule,FormsModule,ToastModule,TableModule,
            ButtonModule,InputTextModule,ToggleButtonModule,RippleModule,
            MultiSelectModule,DropdownModule,ProgressBarModule,SliderModule,RatingModule,
            MessageModule,DialogModule,ToolbarModule],
  templateUrl: './corptable.component.html',
  styleUrl: './corptable.component.scss',
  providers:[MessageService]
})
export class CorptableComponent implements OnInit {
  corps: Corp[] = [];
  users: User[] = [];
  farmerUsers: any[];
  expandedRows: any = {};
  isExpanded: boolean = false;
  submitted: boolean = false;
  addNewCorpDialog: boolean = false;
  deleteCorpDialog: boolean = false;
  newCorp: Corp = {
    name: '',
    latitude: 0 ,
    longitude :0 ,
    owner: null,
  };
  selectedCopr: Corp [] = [];
  cols: any[] = [
      { field: 'name', header: 'Corp Name' },
      { field: 'latitude', header: 'Latitude' },
      { field: 'longitude', header: 'Longitude' },
      { field: 'totalSize', header: 'Total Size' },
      { field: 'ownerName', header: 'Owner Name' }
  ];
  editeCorpDialog: boolean = false;
  rowsPerPageOptions = [10, 20, 50, 100];
  currentUser: any;

  constructor(private corpService: CorpService, private messageService: MessageService, private userService: UserserviceService, private router: Router,private authService: AuthService ) {}

  ngOnInit() {
    this.currentUser = this.authService.getCurrentUser(); // Fetch the current user's information
    if (this.currentUser) {
      this.loadCorpsForCurrentUser(); // Load corporations owned by the current user
      this.loadUsers();
    }
  }

  loadCorpsForCurrentUser() {
    this.corpService.getAllCorps().subscribe(corps => {
      // Filter the corporations based on the owner ID matching the ID of the current user
      this.corps = corps.filter(corp => corp.owner.username === this.currentUser.username);
    });
  }

  loadUsers() {
    this.userService.getUsers().subscribe(
      (users: User[]) => {
        this.users = users;
        this.farmerUsers = this.users.filter(userItem => userItem.roles.some((role: any) => role.name === 'FARMER'));
      },
      (error) => {
        console.error('Error fetching users:', error);
      }
    );
  }

  openNew() {
    this.newCorp = { name: '', latitude: 0 ,longitude :0, owner: null };
    this.submitted = false;
    this.addNewCorpDialog = true;
  }

  deleteSelectedCorp() {
    this.deleteCorpDialog = true;
  }

  saveCorp() {
    this.submitted = true;
    if (this.newCorp.name && this.newCorp.owner && this.newCorp.owner.id) { // Ensure owner is selected
      this.corpService.createCorp(this.newCorp).subscribe(response => {
        this.loadCorpsForCurrentUser();
        this.addNewCorpDialog = false;
        this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Corp created successfully' });
      }, error => {
        console.error('Error creating corp:', error);
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Failed to create corp' });
      });
    }
  }

  editCorp(corp: Corp) {
    this.newCorp = { ...corp };
    this.editeCorpDialog = true;
  }

  deleteCorp(corp: Corp) {
    this.deleteCorpDialog = true;
    this.newCorp = { ...corp };
  }

  updateCorp() {
    this.corpService.updateCorp(this.newCorp.id, this.newCorp).subscribe(() => {
      this.loadCorpsForCurrentUser();
      this.editeCorpDialog = false;
      this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Corp updated successfully' });
    }, error => {
      console.error('Error updating corp:', error);
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Failed to update corp' });
    });
  }

  confirmDelete() {
    this.deleteCorpDialog = false;
    this.corpService.deleteCorpById(this.newCorp.id).subscribe(() => {
      this.loadCorpsForCurrentUser();
    });
  }

  expandAll() {
    if (!this.isExpanded) {
      this.corps.forEach(corp => corp && corp.id ? this.expandedRows[corp.id] = true : '');
    } else {
      this.expandedRows = {};
    }
    this.isExpanded = !this.isExpanded;
  }

  hideDialog() {
    this.addNewCorpDialog = false;
    this.editeCorpDialog = false;
    this.deleteCorpDialog = false;
  }

  onGlobalFilter(dt: any, event: Event) {
    if (dt && event) {
      const filterValue = (event.target as HTMLInputElement).value;
      dt.filterGlobal(filterValue, 'contains');
    }
  }

  navigateToParcelDashboard(parcelName: string) {
    this.router.navigate(['/parceldashboard', { parcelName: parcelName }]);
  }
  
}