import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParceldashboardComponent } from './parceldashboard.component';

describe('ParceldashboardComponent', () => {
  let component: ParceldashboardComponent;
  let fixture: ComponentFixture<ParceldashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ParceldashboardComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ParceldashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
