import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ChartModule } from 'primeng/chart';
import { MenuModule } from 'primeng/menu';
import { TableModule } from 'primeng/table';
import { Subscription } from 'rxjs';
import { SensorData } from 'src/app/model/sensordata';
import { AuthService } from 'src/app/services/auth/auth.service';
import { SensorDataService } from 'src/app/services/sensordata/sensordata.service';
import { WebSocketService } from 'src/app/services/websocket/websocket.service';
import { SharedlayoutComponent } from 'src/app/shared/sharedlayout/sharedlayout.component';
import { ButtonModule } from 'primeng/button';
import { FormsModule } from '@angular/forms';
import { DialogModule } from 'primeng/dialog';
import { InputTextModule } from 'primeng/inputtext';
import { FieldsetModule } from 'primeng/fieldset';
import { MessageModule } from 'primeng/message';
import { MessageService } from 'primeng/api';
import { RippleModule } from 'primeng/ripple';

@Component({
  selector: 'app-parceldashboard',
  standalone: true,
  imports: [CommonModule,FormsModule,TableModule,MenuModule,ChartModule,SharedlayoutComponent,ButtonModule,DialogModule,InputTextModule,FieldsetModule,MessageModule,RippleModule],
  templateUrl: './parceldashboard.component.html',
  styleUrl: './parceldashboard.component.scss',
  providers:[MessageService]
})
export class ParceldashboardComponent implements OnInit {

  soilMoisture: number;
  soilTemperature: number;
  evapotranspiration: number;
  parcelName: string;
  corpName: string;
  userName: string;
  alerts: SensorData[] = [];
  decision: string;
  actionLabel: string = 'Start';
  private wsSubscription: Subscription;

  soilMoistureChartData: any;
  soilTemperatureChartData: any;
  evapotranspirationChartData: any;

  chartOptions: any;

  irrigationScheduleDialog: boolean = false;
  irrigationSchedule = {
    startDate: '',
    startTime: '',
    endDate: '',
    endTime: ''
  };

  mode: string = 'Manuel'; // Default mode
  modeLabel: string = 'Switch to Auto';


  constructor(private route: ActivatedRoute, private webSocketService: WebSocketService,private authService: AuthService,private sensorDataService: SensorDataService,private messageService:MessageService) { }

  ngOnInit(): void {
    const currentUser = this.authService.getCurrentUser();
    this.userName = currentUser.username;

    this.route.paramMap.subscribe(params => {
      this.corpName = params.get('corpName');
      this.parcelName = params.get('parcelName');

      // Load initial values from localStorage
      this.loadFromLocalStorage();

      // Initialize actionLabel based on decision
      this.actionLabel = this.decision === 'ON' ? 'Stop' : 'Start';

      this.wsSubscription = this.webSocketService.connect().subscribe((data: SensorData) => {
        if (data.userName === this.userName && data.corpName === this.corpName && data.parcelName === this.parcelName) {
          if (data.sensorName === 'soil_moisture') {
            this.soilMoisture = Number(data.data);
            this.saveToLocalStorage();
          } else if (data.sensorName === 'soil_temperature') {
            this.soilTemperature = Number(data.data);
            this.saveToLocalStorage();
          } else if (data.sensorName === 'evapotranspiration') {
            this.evapotranspiration = Number(data.data);
            this.saveToLocalStorage();
          } else if (data.sensorName === 'alert') {
            this.alerts.push(data);
            this.saveToLocalStorage();
          } else if (data.sensorName === 'decision') {
            console.log(data);
            const decisionData: string = data.data as unknown as string; // Explicitly specify data.data as string
            const parsedData = JSON.parse(decisionData); // Parse the JSON data
            const decisionState = parsedData[0]; // Get the decision state ("OFF" or "ON")
            this.decision = decisionState === 'ON' ? 'On' : 'Off'; // Set decision to "On" or "Off"
            this.saveToLocalStorage();
          }
        }
      });

      // Fetch sensor data from the service
      this.sensorDataService.getSensorDataByUserCorpAndParcel(this.userName, this.corpName, this.parcelName)
        .subscribe((sensorData: SensorData[]) => {
          this.updateChartData(sensorData);
        });
    });
  }

  ngOnDestroy() {
    if (this.wsSubscription) {
      this.wsSubscription.unsubscribe();
    }
  }

  // Method to load data from localStorage
  loadFromLocalStorage() {
    const soilMoisture = localStorage.getItem('soilMoisture');
    const soilTemperature = localStorage.getItem('soilTemperature');
    const evapotranspiration = localStorage.getItem('evapotranspiration');
    const decision = localStorage.getItem('decision');
    const alerts = localStorage.getItem('alerts');

    if (soilMoisture) {
      this.soilMoisture = parseFloat(soilMoisture);
    }
    if (soilTemperature) {
      this.soilTemperature = parseFloat(soilTemperature);
    }
    if (evapotranspiration) {
      this.evapotranspiration = parseFloat(evapotranspiration);
    }
    if (decision) {
      this.decision = decision;
    }
    if (alerts) {
      this.alerts = JSON.parse(alerts);
    }
  }

  // Method to save data to localStorage
  saveToLocalStorage() {
    localStorage.setItem('soilMoisture', this.soilMoisture.toString());
    localStorage.setItem('soilTemperature', this.soilTemperature.toString());
    localStorage.setItem('evapotranspiration', this.evapotranspiration.toString());
    localStorage.setItem('decision', this.decision);
    localStorage.setItem('alerts', JSON.stringify(this.alerts));
  }

  updateChartData(sensorData: SensorData[]): void {
    const datasets = {
      'Soil Moisture': [],
      'Soil Temperature': [],
      'Evapotranspiration': []
    };
  
    // Group data by sensor name
    sensorData.forEach(data => {
      datasets[data.sensorName].push({
        label: new Date(data.timestamp).toLocaleString('en-US', { month: 'short', day: 'numeric', hour: 'numeric' }),
        data: [data.data]
      });
    });
  
    // Keep only the last 10 data points
    const getLastTenData = (dataArray: any[]) => dataArray.slice(-10);
  
    // Update chart data with only the last 10 data points
    this.soilMoistureChartData = {
      labels: getLastTenData(datasets['Soil Moisture']).map(data => data.label),
      datasets: [{
        label: 'Soil Moisture',
        data: getLastTenData(datasets['Soil Moisture']).map(data => data.data[0]),
        borderColor: this.getRandomColor(),
        fill: false
      }]
    };
  
    this.soilTemperatureChartData = {
      labels: getLastTenData(datasets['Soil Temperature']).map(data => data.label),
      datasets: [{
        label: 'Soil Temperature',
        data: getLastTenData(datasets['Soil Temperature']).map(data => data.data[0]),
        borderColor: this.getRandomColor(),
        fill: false
      }]
    };
  
    this.evapotranspirationChartData = {
      labels: getLastTenData(datasets['Evapotranspiration']).map(data => data.label),
      datasets: [{
        label: 'Evapotranspiration',
        data: getLastTenData(datasets['Evapotranspiration']).map(data => data.data[0]),
        borderColor: this.getRandomColor(),
        fill: false
      }]
    };

    // Limit alerts to the last 5
  this.alerts = this.alerts.slice(-5);
  }
  
  getRandomColor(): string {
    // Function to generate random color for the dataset border
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }
  
  initChart(): void {
    const documentStyle = getComputedStyle(document.documentElement);
    const textColor = documentStyle.getPropertyValue('--text-color');
    const textColorSecondary = documentStyle.getPropertyValue('--text-color-secondary');
    const surfaceBorder = documentStyle.getPropertyValue('--surface-border');
  
    this.chartOptions = {
      plugins: {
        legend: {
          labels: {
            color: textColor
          }
        }
      },
      scales: {
        x: {
          ticks: {
            color: textColorSecondary
          },
          grid: {
            color: surfaceBorder,
            drawBorder: false
          }
        },
        y: {
          ticks: {
            color: textColorSecondary
          },
          grid: {
            color: surfaceBorder,
            drawBorder: false
          }
        }
      }
    };
  }

   // Method to toggle system state
   toggleSystem() {
    this.decision = this.decision === 'ON' ? 'OFF' : 'ON';
    this.updateActionLabel();
    this.saveToLocalStorage();
    this.webSocketService.sendData({
      userName: this.userName,
      corpName: this.corpName,
      parcelName: this.parcelName,
      action: this.decision
    });
  }

  updateActionLabel() {
    this.actionLabel = this.decision === 'ON' ? 'Stop' : 'Start';
  }

  showIrrigationScheduleDialog() {
    this.irrigationScheduleDialog = true;
  }

  submitIrrigationSchedule() {
    this.irrigationScheduleDialog = false;
    this.updateActionLabel
    const scheduleData = {
      userName: this.userName,
      corpName: this.corpName,
      parcelName: this.parcelName,
      schedule: this.irrigationSchedule
    };
    console.log(scheduleData)
    this.webSocketService.sendData(scheduleData);
    this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Planing sended successfully' });
  }

  toggleMode() {
    this.mode = this.mode === 'Auto' ? 'Manual' : 'Auto';
    this.updateModeLabel();
    this.saveToLocalStorage();
    console.log(this.mode)
    this.webSocketService.sendData({
      userName: this.userName,
      corpName: this.corpName,
      parcelName: this.parcelName,
      mode: this.mode
    });
  }

  updateModeLabel() {
    this.modeLabel = this.mode === 'Auto' ? 'Switch to Manual' : 'Switch to Auto';
  }

  
}