import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MessageService } from 'primeng/api';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { MessageModule } from 'primeng/message';
import { MultiSelectModule } from 'primeng/multiselect';
import { ProgressBarModule } from 'primeng/progressbar';
import { RatingModule } from 'primeng/rating';
import { RippleModule } from 'primeng/ripple';
import { SliderModule } from 'primeng/slider';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { ToolbarModule } from 'primeng/toolbar';
import { Corp } from 'src/app/model/corp';
import { ESoilType } from 'src/app/model/esoiltype';
import { Parcelle } from 'src/app/model/parcelle';
import { Plant } from 'src/app/model/plant';
import { AuthService } from 'src/app/services/auth/auth.service';
import { CorpService } from 'src/app/services/corp/corp.service';
import { ParcelleService } from 'src/app/services/parcelle/parcelle.service';
import { PlantService } from 'src/app/services/plant/plant.service';

@Component({
  selector: 'app-parcelle',
  standalone: true,
  imports: [CommonModule,RouterModule,FormsModule,ToastModule,TableModule,ButtonModule,InputTextModule,ToggleButtonModule,RippleModule,MultiSelectModule,DropdownModule,ProgressBarModule,SliderModule,RatingModule,MessageModule,DialogModule,ToolbarModule],
  templateUrl: './parcelle.component.html',
  styleUrl: './parcelle.component.scss',
  providers :[MessageService]
})
export class ParcelleComponent implements OnInit {
  parcelles: Parcelle[] = [];
  parcelleCols: any[] = [
    { field: 'name', header: 'Name' },
    { field: 'soilType', header: 'Soil Type' },
    { field: 'size', header: 'Size' },
    { field: 'plant', header: 'Plant' },
    { field: 'corpName', header: 'Corp' },
  ];
  selectedParcelle: Parcelle[] = [];
  rowsPerPageOptions = [10, 20, 50, 100];
  parcelleDialog: boolean = false;
  newParcelle: Parcelle = {
    name: '',
    soilType: null,
    size: 0,
    plant: null,
    corp: null
  };
  soilTypes: any[] = Object.values(ESoilType);
  plant: Plant[] = [];
  corp: Corp[] = [];
  addNewParcelleDialog: boolean = false;
  deleteParcelleDialog: boolean = false;
  editParcelleDialog: boolean = false;
  submitted: boolean = false;
  currentUser: any;

  constructor(private parcelleService: ParcelleService, private messageService: MessageService,private plantService: PlantService,
    private corpService: CorpService, private authService: AuthService) { }

  ngOnInit(): void {
    this.currentUser = this.authService.getCurrentUser(); // Fetch the current user's information
    if (this.currentUser) {
      this.loadParcelles(); // Load parcels owned by the current user
      this.loadPlants();
      this.loadCorps();
    }
  }

  loadCorps() {
    this.corpService.getAllCorps().subscribe(
      (corps: Corp[]) => {
        this.corp = corps;
      },
      (error) => {
        console.error('Error fetching corporations:', error);
      }
    );
  }

  loadPlants() {
    this.plantService.getAllPlants().subscribe(
      (plants: Plant[]) => {
        this.plant = plants;
      },
      (error) => {
        console.error('Error fetching plants:', error);
      }
    );
  }

  loadParcelles() {
    // First, fetch all corps
    this.corpService.getAllCorps().subscribe(
      (corps: Corp[]) => {
        // Filter the corps to get only the ones owned by the current user
        const userOwnedCorps = corps.filter(corp => corp.owner.username === this.currentUser.username);
        // Extract parcelles from user-owned corps
        this.parcelles = [];
        userOwnedCorps.forEach(corp => {
          if (corp.parcelleList && corp.parcelleList.length > 0) {
            this.parcelles.push(...corp.parcelleList);
          }
        });
        
      },
      (error) => {
        console.error('Error fetching corporations:', error);
      }
    );
  }

  openNew(){
    this.newParcelle = { name: '', soilType: null, size: 0, plant: null, corp: null };
    this.submitted = false;
    this.addNewParcelleDialog = true;
  }

  hideDialog(){
    this.addNewParcelleDialog = false;
    this.editParcelleDialog = false ;
    this.deleteParcelleDialog = false ;
  }

  saveParcelle() {
    if (this.newParcelle.corp) { // Check if Corp object is selected
      this.parcelleService.createParcelle(this.newParcelle).subscribe(
        (response) => {
          this.loadParcelles();
          this.addNewParcelleDialog = false;
          this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Parcelle added successfully' });
        },
        (error) => {
          console.error('Error adding parcelle:', error);
          this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Failed to add parcelle' });
        }
      );
    } else {
      console.error('Corp object is not selected');
    }
  }

  deleteParcelle(parcelle: Parcelle) {
    this.deleteParcelleDialog = true;
    this.newParcelle = { ...parcelle };
  }

  editParcelle(parcelle: Parcelle) {
    this.newParcelle = { ...parcelle };
    this.editParcelleDialog = true;
  }

  updateParcelle(){
    this.parcelleService.updateParcelle(this.newParcelle.id, this.newParcelle).subscribe(() => {
      this.loadParcelles();
      this.editParcelleDialog = false;
      this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Parcelle updated successfully' });
    }, error => {
      console.error('Error updating corp:', error);
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Failed to update Parcelle' });
    });
  }

  confirmDelete() {
    this.deleteParcelleDialog = false;
    this.parcelleService.deleteParcelleById(this.newParcelle.id).subscribe(() => {
      this.loadParcelles();
    });
  }

  onGlobalFilter(dt: any, event: Event): void {
    if (dt && event) {
      const filterValue = (event.target as HTMLInputElement).value;
      dt.filterGlobal(filterValue, 'contains');
    }
  }
}