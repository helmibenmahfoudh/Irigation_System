import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MessageService } from 'primeng/api';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { MessageModule } from 'primeng/message';
import { MultiSelectModule } from 'primeng/multiselect';
import { ProgressBarModule } from 'primeng/progressbar';
import { RatingModule } from 'primeng/rating';
import { RippleModule } from 'primeng/ripple';
import { SliderModule } from 'primeng/slider';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { ToolbarModule } from 'primeng/toolbar';
import { Plant } from 'src/app/model/plant';
import { PlantService } from 'src/app/services/plant/plant.service';

@Component({
  selector: 'app-plant',
  standalone: true,
  imports: [CommonModule,RouterModule,FormsModule,ToastModule,TableModule,ButtonModule,InputTextModule,ToggleButtonModule,RippleModule,MultiSelectModule,DropdownModule,ProgressBarModule,SliderModule,RatingModule,MessageModule,DialogModule,ToolbarModule],
  templateUrl: './plant.component.html',
  styleUrl: './plant.component.scss',
  providers:[MessageService]
})
export class PlantComponent implements OnInit {

  plants: Plant[] = [];
  newPlant: Plant = {
    name: '',
  };
  plantCols: any[] = [ // Columns for the plant table
    { field: 'name', header: 'Name' },
  ];
  submitted: boolean = false;
  selectedPlants: Plant[] = [];
  addPlantDialog: boolean = false;
  editPlantDialog: boolean = false;
  deletePlantDialog: boolean = false;
  rowsPerPageOptions = [10, 20, 50, 100];

  constructor(private plantService: PlantService, private messageService: MessageService) { }

  ngOnInit(): void {
    this.loadPlants();
  }

  loadPlants(): void {
    this.plantService.getAllPlants().subscribe(plants => {
      this.plants = plants;
    });
  }

  openNewPlant() {
    this.newPlant = {
      name: '',
    };
    this.submitted = false;
    this.addPlantDialog = true;
  }

  savePlant() {
    if (this.newPlant.name) {
      this.plantService.createPlant(this.newPlant).subscribe(response => {
        this.loadPlants();
        this.addPlantDialog = false;
      });
    }
  }

  editPlant(plant: Plant) {
    this.newPlant = { ...plant };
    this.editPlantDialog = true;
  }

  updatePlant() {
    if (this.newPlant.id) {
      this.plantService.updatePlant(this.newPlant.id, this.newPlant).subscribe(() => {
        this.loadPlants();
        this.editPlantDialog = false;
        this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Plant updated successfully' });
      }, error => {
        console.error('Error updating plant:', error);
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Failed to update plant' });
      });
    }
  }

  deletePlant(plant: Plant) {
    this.deletePlantDialog = true;
    this.newPlant = plant;
  }

  confirmDeletePlant() {
    if (this.newPlant.id) {
      this.plantService.deletePlant(this.newPlant.id).subscribe(() => {
        this.loadPlants();
        this.deletePlantDialog = false;
      });
    }
  }

  onGlobalFilter(dt: any, event: Event) {
    if (dt && event) {
      const filterValue = (event.target as HTMLInputElement).value;
      dt.filterGlobal(filterValue, 'contains');
    }
  }

  hideDialog() {
    this.addPlantDialog = false;
    this.editPlantDialog = false;
    this.deletePlantDialog = false;
  }

}