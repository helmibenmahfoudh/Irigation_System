import { Parcelle } from "./parcelle";
import { User } from "./user";

export interface Corp {
    id?: string;
    name: string;
    latitude: number;
    longitude: number;
    totalSize ?: number;
    parcelleList?: Parcelle[];
    owner : User ;
  }