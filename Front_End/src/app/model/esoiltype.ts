export enum ESoilType {
    SANDY_LOAM = 'SANDY_LOAM',
    CLAY = 'CLAY',
    LOAM = 'LOAM',
    SANDY = 'SANDY',
    SILT = 'SILT',
    PEAT_MOSS = 'PEAT_MOSS',
    CHALK = 'CHALK'
  }