interface LogEntry {
    level: string; // Assuming your logs have a 'level' property
    message: string;
  }