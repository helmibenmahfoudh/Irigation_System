import { Corp } from "./corp";
import { ESoilType } from "./esoiltype";
import { Plant } from "./plant";

export interface Parcelle {
    id?: string;
    name: string;
    soilType: ESoilType;
    size: number;
    plant: Plant;
    corp: Corp; // Assuming you also want to access the Corp object directly
    corpName?: string;
  }