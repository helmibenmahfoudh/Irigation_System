export interface SensorData {
  id: string;
  sensorName: string;
  data: number;
  timestamp: string;
  parcelName: string;
  corpName: string; 
  userName : string;
}
