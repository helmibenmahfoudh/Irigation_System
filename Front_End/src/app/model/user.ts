import { Corp } from "./corp";

export interface User {
    id?: string;
    username: string;
    email: string;
    password: string;
    firstName?: string;
    lastName?: string;
    createdBy?: string;
    roles?: string[]; // If you have roles associated with users
    corp ?: Corp ;
  }
  