import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private apiUrl = environment.auth;

  private isAuthenticated = false;
  private authSecretKey = 'Bearer Token';
  private jwtHelper = new JwtHelperService();
  
constructor(private http: HttpClient) {
  this.isAuthenticated = !!sessionStorage.getItem(this.authSecretKey);
}

  login(username: string, password: string, remember: boolean): Observable<boolean> {
    return this.http.post(`${this.apiUrl}/signin`, { 'username': username, 'password': password, 'remember': remember }).pipe(
      map((resp: any) => {
        sessionStorage.setItem(this.authSecretKey, resp.token);
        this.isAuthenticated = true;
        return true;
      }),
      catchError((error: any) => {
        return of(false);
      })
    );
  }

  signup(username: string, email: string, password: string, firstName: string, lastName: string): Observable<boolean> {
    return this.http.post(`${this.apiUrl}/signup`, {
      'username': username,
      'email': email,
      'password': password,
      'firstName': firstName,
      'lastName': lastName
    }).pipe(
      map((resp: any) => {
        console.log(resp)
        return true;
      }),
      catchError((error: any) => {
        console.log(error)
        return of(false);
      })
    );
  }

  getToken(): string | null {
    return sessionStorage.getItem(this.authSecretKey)
  }

  isAuthenticatedUser(): boolean {
    return this.isAuthenticated;
  }

  getUserRoles(): string[] {
    const token = sessionStorage.getItem(this.authSecretKey);
    if (token) {
      const decodedToken = this.jwtHelper.decodeToken(token);
      return decodedToken.roles.split(',').map((role: string) => role.trim()); // Split roles and trim spaces
    }
    return [];
  }

  getCurrentUser(): any {
    const token = sessionStorage.getItem(this.authSecretKey);
    if (token) {
      const decodedToken = this.jwtHelper.decodeToken(token);
      return {
        username: decodedToken.sub,
        roles: Array.isArray(decodedToken.roles) ? decodedToken.roles : [decodedToken.roles]
      };
    }
    return null;
  }

  logout(): void {
    sessionStorage.removeItem(this.authSecretKey);
    this.isAuthenticated = false;
  }
}
