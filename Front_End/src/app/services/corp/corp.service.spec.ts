import { TestBed } from '@angular/core/testing';

import { CorpService } from './corp.service';

describe('CorpService', () => {
  let service: CorpService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CorpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
