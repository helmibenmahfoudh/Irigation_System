import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Corp } from 'src/app/model/corp';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CorpService {
  private apiUrl = environment.apiCorp // This should match your backend API URL

  constructor(private http: HttpClient) { }

  createCorp(corp: Corp): Observable<Corp> {
    return this.http.post<Corp>(this.apiUrl, corp);
  }

  getCorpById(corpId: string): Observable<Corp> {
    const url = `${this.apiUrl}/${corpId}`;
    return this.http.get<Corp>(url);
  }

  getAllCorps(): Observable<Corp[]> {
    return this.http.get<Corp[]>(this.apiUrl);
  }

  updateCorp(corpId: string, updatedCorp: Corp): Observable<Corp> {
    const url = `${this.apiUrl}/${corpId}`;
    return this.http.put<Corp>(url, updatedCorp);
  }

  deleteCorpById(corpId: string): Observable<void> {
    const url = `${this.apiUrl}/${corpId}`;
    return this.http.delete<void>(url);
  }
}