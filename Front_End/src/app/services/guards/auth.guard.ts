import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router } from "@angular/router";
import { AuthService } from "../auth/auth.service";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) {}

  canActivate(next: ActivatedRouteSnapshot): boolean {
    return this.checkAuth(next);
  }

  private checkAuth(route: ActivatedRouteSnapshot): boolean {
    if (!this.authService.isAuthenticatedUser()) {
      // Redirect to the login page if the user is not authenticated
      this.router.navigate(['/login']);
      return false;
    }

    const requiredRoles = route.data["roles"] as string[];
    if (requiredRoles && requiredRoles.length > 0) {
      const userRoles = this.authService.getUserRoles();
      if (!userRoles.some(role => requiredRoles.includes(role))) {
        // Redirect to unauthorized page if user does not have any of the required roles
        this.router.navigate(['/unauthorized']);
        return false;
      }
    }

    return true;
  }
}
