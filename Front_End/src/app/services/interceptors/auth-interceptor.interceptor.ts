import { HttpInterceptorFn } from '@angular/common/http';

export const authInterceptorInterceptor: HttpInterceptorFn = (req, next) => {
  const token = sessionStorage.getItem('Bearer Token')
  if (token) {
    const newReq = req.clone({ setHeaders: { 'Authorization': 'Bearer ' + token } });
    return next(newReq);
  } else {
    return next(req);
  }
};
