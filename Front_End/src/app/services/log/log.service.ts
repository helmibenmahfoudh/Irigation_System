import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LogService {
  private apiUrl = 'http://localhost:8080/api/logs'; // Replace with your backend API URL

  constructor(private http: HttpClient) {}

  getLogs(): Observable<string[]> {
    return this.http.get<string[]>(this.apiUrl);
  }
}