import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { Parcelle } from 'src/app/model/parcelle';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ParcelleService {
  private apiUrl = environment.apiParcelle; // This should match your backend API URL

  constructor(private http: HttpClient) { }

  createParcelle(parcelle: Parcelle): Observable<Parcelle> {
    return this.http.post<Parcelle>(this.apiUrl, parcelle);
  }

  getParcelleById(parcelleId: string): Observable<Parcelle> {
    const url = `${this.apiUrl}/${parcelleId}`;
    return this.http.get<Parcelle>(url);
  }

  getAllParcelles(): Observable<Parcelle[]> {
    return this.http.get<Parcelle[]>(this.apiUrl).pipe(
      map((parcelles: Parcelle[]) => {
        return parcelles.map((parcelle) => {
          return {
            ...parcelle,
            corpName: parcelle.corp ? parcelle.corp.name : null,
          };
        });
      })
    );
  }

  updateParcelle(parcelleId: string, updatedParcelle: Parcelle): Observable<Parcelle> {
    const url = `${this.apiUrl}/${parcelleId}`;
    return this.http.put<Parcelle>(url, updatedParcelle);
  }

  deleteParcelleById(parcelleId: string): Observable<void> {
    const url = `${this.apiUrl}/${parcelleId}`;
    return this.http.delete<void>(url);
  }
}
