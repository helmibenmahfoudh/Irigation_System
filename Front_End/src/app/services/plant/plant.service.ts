import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Plant } from 'src/app/model/plant';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PlantService {

  private apiUrl = environment.apiPlant;

  constructor(private http: HttpClient) { }

  // Get all plants
  getAllPlants(): Observable<Plant[]> {
    return this.http.get<Plant[]>(this.apiUrl);
  }

  // Get plant by ID
  getPlantById(id: string): Observable<Plant> {
    const url = `${this.apiUrl}/${id}`;
    return this.http.get<Plant>(url);
  }

  // Create a new plant
  createPlant(plant: Plant): Observable<Plant> {
    return this.http.post<Plant>(this.apiUrl, plant);
  }

  // Update an existing plant
  updatePlant(id: string, plant: Plant): Observable<Plant> {
    const url = `${this.apiUrl}/${id}`;
    return this.http.put<Plant>(url, plant);
  }

  // Delete a plant
  deletePlant(id: string): Observable<any> {
    const url = `${this.apiUrl}/${id}`;
    return this.http.delete(url);
  }
}