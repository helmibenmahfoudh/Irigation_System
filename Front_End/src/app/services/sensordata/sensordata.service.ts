import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, map } from 'rxjs';
import { SensorData } from 'src/app/model/sensordata';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class SensorDataService {

  private apiUrl = environment.apiSensor; // Update with your backend API URL

  constructor(private http: HttpClient) { }

  getSensorDataByUserCorpAndParcel(userName: string, corpName: string, parcelName: string): Observable<SensorData[]> {
    return this.http.get<SensorData[]>(`${this.apiUrl}/${userName}/${corpName}/${parcelName}`);
  }
}