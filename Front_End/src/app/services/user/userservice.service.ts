import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from 'src/app/model/user';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserserviceService {

  apiUrl = environment.apiUsers;
  apiAuth = environment.auth;

  constructor(private http: HttpClient) {}

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.apiUrl);
  }
  

  updateUser(id: string, user: User): Observable<void> {
    const url = `${this.apiUrl}/${id}`;
    return this.http.put<void>(url, user);
  }

  deleteUser(userId: string | string[]): Observable<void> {
    const url = `${this.apiUrl}/${Array.isArray(userId) ? 'delete-multiple' : userId}`;
    return this.http.delete<void>(url);
  }
}
