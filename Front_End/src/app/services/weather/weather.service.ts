import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  constructor(private http: HttpClient) { }

  findPlacesByName(query: string): Observable<any> {
    let url = `https://nominatim.openstreetmap.org/search?format=json&q=${query}`;
    return this.http.get(url);
  }
  findPlaceByCoordinations(lat: number, lon: number): Observable<any> {
    let url = `https://nominatim.openstreetmap.org/reverse?format=json&lat=${lat}&lon=${lon}`;
    return this.http.get(url);
  }

  getWeather(lat: number, lon: number): Observable<any> {
    let url = `https://api.open-meteo.com/v1/forecast?latitude=${lat}&longitude=${lon}&current=temperature_2m,is_day,precipitation,cloud_cover,wind_speed_10m,wind_direction_10m`;
    return this.http.get(url);
  }

  getWeaterDetail(lat: number, lon: number): Observable<any> {
    let url = `https://api.open-meteo.com/v1/forecast?latitude=${lat}&longitude=${lon}&hourly=temperature_2m,wind_speed_10m&forecast_days=16`;
    return this.http.get(url);
  }

  getPrecipitation(lat: number, lon: number): Observable<any> {
    let url = `https://api.open-meteo.com/v1/forecast?latitude=${lat}&longitude=${lon}&daily=precipitation_sum`;
    return this.http.get(url);
  }
}