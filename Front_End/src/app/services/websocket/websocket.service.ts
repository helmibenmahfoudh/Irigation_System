import { Injectable } from '@angular/core';
import { io, Socket } from 'socket.io-client';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class WebSocketService {
  private socket: Socket;

  constructor() {
    console.log('WebSocketService: Connecting to Socket.IO server');
    this.socket = io('http://localhost:3000');
    this.socket.on('connect', () => {
      console.log('WebSocketService: Connected to Socket.IO server');
      this.socket.on('sensor_data', (data: any) => {
        //console.log(data)
      });
    });
  }

  connect(): Observable<any> {
    return new Observable<any>(observer => {
      this.socket.on('sensor_data', (data: any) => {
        observer.next(data);
      });
    });
  }
  
  disconnect() {
    this.socket.disconnect();
  }

  sendData(data: any) {
    // Send data to the server
    this.socket.emit('sended_data', data);
  }
}
