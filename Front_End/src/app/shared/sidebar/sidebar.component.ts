import { Component, ElementRef, OnInit } from '@angular/core';
import { LayoutService } from '../service/app.layout.service';
import { CommonModule } from '@angular/common';
import { Router, RouterModule } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { InputTextModule } from 'primeng/inputtext';
import { SidebarModule } from 'primeng/sidebar';
import { BadgeModule } from 'primeng/badge';
import { RadioButtonModule } from 'primeng/radiobutton';
import { InputSwitchModule } from 'primeng/inputswitch';
import { RippleModule } from 'primeng/ripple';

@Component({
  selector: 'app-sidebar',
  standalone: true,
  imports: [CommonModule,RouterModule,FormsModule,HttpClientModule,InputTextModule,SidebarModule,BadgeModule,RadioButtonModule,InputSwitchModule,RippleModule],
  templateUrl: './sidebar.component.html',
  styleUrl: './sidebar.component.scss'
})
export class SidebarComponent implements OnInit {

  model: any[] = [];
  currentUser: any;

  constructor(public layoutService: LayoutService, private router: Router, private authService:AuthService,public el: ElementRef) { }

  ngOnInit() {
    this.currentUser = this.authService.getUserRoles();
  // Call a method to filter the menu based on role after fetching user
    this.filterMenuByRole(this.currentUser);
  }

  filterMenuByRole(roles: string[]): void {
   // Define allowed roles (adjust as needed)
    let menu = [];

    const userRole = roles && roles.length > 0 ? roles[0] : null; // Get first role or set to null if roles is empty/undefined
    if (!userRole) return; // Find a valid role
  
    switch (userRole) {
      case 'ADMIN':
        menu = [
          {
            items: [
              { label: 'User Table', icon: 'pi pi-fw pi-table', routerLink: ['/users'] }
            ]
          },
          {
            items: [
              { label: 'Logs', icon: 'fas fa-file', routerLink: ['/admin/logs'] }
            ]
          },
          // Admin menu items
        ];
        break;
      case 'FARMER':
        menu = [
          {
            items: [
              { label: 'Crops', icon: 'fas fa-leaf', routerLink: ['/farmer'] },
              { label: 'Plants', icon: 'fas fa-seedling', routerLink: ['/farmer/plants'] },
              { label: 'Parcelles', icon: 'fas fa-map-marked-alt', routerLink: ['/farmer/parcelles']},
              { label: 'User Table', icon: 'pi pi-fw pi-table', routerLink: ['/users'] }
              
            ]
          },
          // Additional Farmer menu items can be added here
        ];
        break;
      case 'EMPLOYEE':
        menu = [
          // Employee menu items
        ];
        break;
      default:
        menu = []; // Hide menu for unknown roles
    }
  
    this.model = menu;
  }

  navigate(item: any): void {
    if (item.routerLink) {
      this.router.navigate(item.routerLink); // Use Router to navigate on click
    }
  }
}
