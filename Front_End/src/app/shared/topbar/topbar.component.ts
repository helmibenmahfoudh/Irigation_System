import { Component, ElementRef, ViewChild } from '@angular/core';
import { LayoutService } from '../service/app.layout.service';
import { CommonModule } from '@angular/common';
import { Router, RouterModule } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Subscription } from 'rxjs';
import { WebSocketService } from 'src/app/services/websocket/websocket.service';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { OverlayPanel } from 'primeng/overlaypanel';

  @Component({
    selector: 'app-topbar',
    standalone: true,
    imports: [CommonModule,RouterModule,OverlayPanelModule],
    templateUrl: './topbar.component.html',
    styleUrl: './topbar.component.scss'
  })
  export class TopbarComponent {

    items!: MenuItem[];
    notifications: string[] = [];
    private subscription!: Subscription;
    private currentUser: any;


      @ViewChild('menubutton') menuButton!: ElementRef;

      @ViewChild('topbarmenubutton') topbarMenuButton!: ElementRef;

      @ViewChild('topbarmenu') menu!: ElementRef;

      @ViewChild('op') op!: OverlayPanel;

      constructor(
        public layoutService: LayoutService,
        private authService: AuthService,
        private router: Router,
        private webSocketService: WebSocketService
      ) {}
    
      ngOnInit(): void {
        this.currentUser = this.authService.getCurrentUser();
        if (this.currentUser) {
          this.subscription = this.webSocketService.connect().subscribe(data => {
            if (data.sensorName === 'decision' && data.data) {
              if (data.userName === this.currentUser.username && data.corpName && data.parcelName) {
                const parsedData = JSON.parse(data.data); // Parse the JSON string
                const messages = parsedData[1]; // Extract the messages array
                
                // Loop through all messages and add each one to the notifications array
                messages.forEach((message: string) => {
                  const notification = `${message} in ${data.corpName} parcel ${data.parcelName}`;
                  this.notifications.push(notification);
                });
              }
            }
          });
        }
      }

  logout(): void {
    this.authService.logout();
    this.router.navigate(['']);
  }

  // Method to check if user is admin
  isAdmin(): boolean {
    const roles = this.authService.getUserRoles();
    return roles.includes('ADMIN');
  }

  // Method to check if user is farmer or employee
  isFarmerOrEmployee(): boolean {
    const roles = this.authService.getUserRoles();
    return roles.includes('FARMER') || roles.includes('EMPLOYEE');
  }
  }
