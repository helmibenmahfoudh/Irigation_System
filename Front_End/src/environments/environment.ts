// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  apiUrl: "http://localhost:8080/api",
  apiUsers: "http://localhost:8080/api/users",
  apiPlant : "http://localhost:8080/api/plants",
  apiParcelle : "http://localhost:8080/api/parcelles",
  apiCorp : "http://localhost:8080/api/corps",
  apiSensor : "http://localhost:8080/api/sensor-data",
  urlBack: "http://localhost:8081/agrotech-api",
  auth: "http://localhost:8080/api/auth",
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
