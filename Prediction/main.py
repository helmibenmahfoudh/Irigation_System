import socketio
import numpy as np
import joblib
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from flask import Flask
import tensorflow as tf
from tensorflow.keras.models import load_model
from tensorflow.keras.initializers import Orthogonal
from tensorflow.keras.utils import get_custom_objects
print(tf.__version__)

app = Flask(__name__)

# Chemins vers le modèle LSTM et le scaler
model_path = r'model_lstm.h5'
scaler_path = r'scaler.pkl'

# Charger le modèle LSTM avec custom_object_scope pour Orthogonal initializer
get_custom_objects().update({'Orthogonal': Orthogonal})

try:
    model = load_model(model_path)
except Exception as e:
    print(f"Error loading model: {e}")

scaler = joblib.load(scaler_path)

# Initialiser le client Socket.IO
sio = socketio.Client()

# Stocker temporairement les valeurs des capteurs
sensor_data = {}

@sio.event
def connect():
    print("Connected to the server")

@sio.event
def disconnect():
    print("Disconnected from the server")

@sio.on('sensor_data')
def handle_sensor_data(data):
    # Imprimer les données reçues
    print(f"Data received: {data}")

    user_name = data['userName']
    corp_name = data['corpName']
    parcel_name = data['parcelName']
    sensor_name = data['sensorName']
    sensor_value = float(data['data'])  # Convertir en float

    # Ajouter les données du capteur reçues à l'objet sensor_data
    if parcel_name not in sensor_data:
        sensor_data[parcel_name] = {sensor_name: [] for sensor_name in ['soil_moisture', 'soil_temperature', 'evapotranspiration']}
    sensor_data[parcel_name][sensor_name].append(sensor_value)

    # Imprimer les données collectées
    print(sensor_data)

    # Vérifier si nous avons suffisamment de données pour faire une prédiction
    required_sensors = ['soil_moisture', 'soil_temperature', 'evapotranspiration']
    if all(len(sensor_data[parcel_name][sensor]) >= 5 for sensor in required_sensors):  # Minimum de 5 valeurs pour LSTM
        # Récupérer les valeurs des capteurs et les convertir en DataFrame
        df = pd.DataFrame(sensor_data[parcel_name])

        # Imprimer le DataFrame avant la normalisation
        print(f"DataFrame before normalization for {parcel_name}:")
        print(df)

        # Normaliser les valeurs
        normalized_values = scaler.transform(df.values)

        # Imprimer les valeurs normalisées
        print(f"Normalized values for {parcel_name}:")
        print(normalized_values)

        # Préparer les données pour la prédiction LSTM
        if normalized_values.shape[0] >= 5:
            sequence = normalized_values[-5:].reshape(1, 5, len(required_sensors))  # Utiliser les 5 dernières valeurs
            predictions_prob = model.predict(sequence)
            predictions = (predictions_prob > 0.5).astype(int)
            sio.emit('prediction', {'userName': user_name, 'corpName': corp_name, 'parcelName': parcel_name, 'prediction': predictions.tolist()})
            print(f"Prediction sent for {user_name}, {corp_name}, {parcel_name}: {predictions.tolist()}")
        else:
            print("Not enough data for prediction. Waiting for more sensor data...")

        # Réinitialiser les données du capteur pour éviter les doublons
        sensor_data[parcel_name] = {sensor_name: [] for sensor_name in required_sensors}

# Se connecter au serveur Node.js
sio.connect('http://localhost:3000')

# Garder le client en marche
sio.wait()
