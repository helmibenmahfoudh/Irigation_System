const express = require('express');
const http = require('http');
const socketIo = require('socket.io');
const mqtt = require('mqtt');

const app = express();
const server = http.createServer(app);
const io = socketIo(server, {
  cors: {
    origin: ["http://34.79.243.169", "http://35.187.89.124:8080"],
    methods: ["GET", "POST"],
    credentials: true
  }
});

const PORT = process.env.PORT || 3000;

// MQTT broker configuration
const brokerUrl = 'mqtt://35.240.75.59:1883'; // Replace with your broker address
const mqttClient = mqtt.connect(brokerUrl);

// Maintain a map of connected users with their usernames
let connectedUsers = new Map();

// Socket.io events
io.on('connection', (socket) => {
  console.log(`A user connected with ID: ${socket.id}`);

  // Listen for setUsername event to set the username for the connected user
  socket.on('setUsername', (username) => {
    connectedUsers.set(socket.id, username);
    // Send updated list of connected users with usernames to all clients
    io.emit('users', Array.from(connectedUsers).map(([id, username]) => ({ id, username })));
  });

  // Listen for incoming messages
  socket.on('message', (data) => {
    const { recipient, message } = data;
    io.to(recipient).emit('message', { sender: socket.id, username: connectedUsers.get(socket.id), message });
    console.log(`Received message from ${socket.id}:`, data); // Log received message
  });

  // Listen for predictions
socket.on('prediction', (data) => {
    const { userName, corpName, parcelName, prediction } = data;
    console.log(`Received prediction for user ${userName}, corp ${corpName}, parcel ${parcelName}:`, prediction);

    // Construct the MQTT topic path
    const topic = `${userName}/${corpName}/${parcelName}/prediction`;

    // Publish the prediction to MQTT
    const message = JSON.stringify({ userName, corpName, parcelName, prediction });

    mqttClient.publish(topic, message, (err) => {
        if (err) {
            console.error('Error publishing prediction to MQTT:', err);
        } else {
            console.log(`Published prediction to MQTT topic ${topic}:`, message);
        }
    });

    // Example: emit the prediction to all connected clients
    io.emit('prediction', { userName, corpName, parcelName, prediction });
});

// Listen for sended Data
  socket.on('sended_data', (data) => {

    if(data.action){
      const { userName, corpName, parcelName, action } = data;
    console.log(`Received action for user ${userName}, corp ${corpName}, parcel ${parcelName}:`, action);

    // Construct the MQTT topic path
    const topic = `${userName}/${corpName}/${parcelName}/action`;

    // Publish the action to MQTT
    mqttClient.publish(topic, action, (err) => {
      if (err) {
        console.error('Error publishing action to MQTT:', err);
      } else {
        console.log(`Published action to MQTT topic ${topic}:`, action);
      }
    });
    }if (data.schedule) {
      const { userName, corpName, parcelName,schedule } = data;
    console.log(`Received schedule for user ${userName}, corp ${corpName}, parcel ${parcelName}:`, schedule);

    // Construct the MQTT topic path
    const topic = `${userName}/${corpName}/${parcelName}/schedule`;

    // Convert schedule to a JSON string
    const scheduleString = JSON.stringify(schedule);

    // Publish the action to MQTT
    mqttClient.publish(topic, scheduleString, (err) => {
      if (err) {
        console.error('Error publishing action to MQTT:', err);
      } else {
        console.log(`Published action to MQTT topic ${topic}:`, scheduleString);
      }
    });
    }if (data.mode) {
      const { userName, corpName, parcelName,mode } = data;
    console.log(`Received schedule for user ${userName}, corp ${corpName}, parcel ${parcelName}:`, mode);

    // Construct the MQTT topic path
    const topic = `${userName}/${corpName}/${parcelName}/mode`;

    // Publish the action to MQTT
    mqttClient.publish(topic, mode, (err) => {
      if (err) {
        console.error('Error publishing action to MQTT:', err);
      } else {
        console.log(`Published action to MQTT topic ${topic}:`, mode);
      }
    });
    } 
    
  });


  // Listen for disconnection
  socket.on('disconnect', () => {
    console.log('User disconnected');
    connectedUsers.delete(socket.id);
    // Send updated list of connected users with usernames to all clients
    io.emit('users', Array.from(connectedUsers).map(([id, username]) => ({ id, username })));
  });
});

// MQTT client events
mqttClient.on('connect', () => {
  console.log('Connected to MQTT broker');

  // Subscribe to MQTT topics with wildcards
  mqttClient.subscribe('+/+/+/soil_moisture', (err) => {
    if (err) {
      console.error('Error subscribing to soil_moisture topic:', err);
    } else {
      console.log('Subscribed to soil_moisture topic');
    }
  });

  mqttClient.subscribe('+/+/+/soil_temperature', (err) => {
    if (err) {
      console.error('Error subscribing to soil_temperature topic:', err);
    } else {
      console.log('Subscribed to soil_temperature topic');
    }
  });

  mqttClient.subscribe('+/+/+/evapotranspiration', (err) => {
    if (err) {
      console.error('Error subscribing to evapotranspiration topic:', err);
    } else {
      console.log('Subscribed to evapotranspiration topic');
    }
  });
mqttClient.subscribe('+/+/+/decision', (err) => {
    if (err) {
      console.error('Error subscribing to soil_moisture topic:', err);
    } else {
      console.log('Subscribed to decision topic');
    }
  });
mqttClient.subscribe('+/+/+/alert', (err) => {
    if (err) {
      console.error('Error subscribing to soil_moisture topic:', err);
    } else {
      console.log('Subscribed to alert topic');
    }
  });
});

mqttClient.on('message', (topic, message) => {
  console.log(`Received message on topic ${topic}: ${message.toString()}`);
  const topicParts = topic.split('/');
  if (topicParts.length >= 4) { // Adjusting to match the new path structure
    const userName = topicParts[0];
    const corpName = topicParts[1];
    const parcelName = topicParts[2];
    const sensorName = topicParts[3];
    const data = message.toString();
    const timestamp = new Date();

    console.log(`Received ${sensorName} data for user ${userName}, corp ${corpName}, parcel ${parcelName}: ${data} at ${timestamp}`);

    // Emit data with user name, corp name, and parcel name included
    io.emit('sensor_data', { userName, corpName, parcelName, sensorName, data });
    console.log('Emitted sensor data successfully');
  } else {
    console.log('Invalid topic format:', topic);
  }
});

server.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
